<?php
	session_start();
	include('connect.php');
	$user=$_SESSION['user_id'];
  $class=$_SESSION['class_id'];
  $query="SELECT * FROM `feedback` WHERE `user_id`='$user'";
  $result = mysqli_query($conn, $query);
?>
<!DOCTYPE html>
<html>
<head>
  <title>Scorion | Dashboard</title>
  <script defer src="https://use.fontawesome.com/releases/v5.0.7/js/all.js"></script>
  <link rel="stylesheet" href="../PHP/css/style.css">
  <link rel="stylesheet" href="../PHP/css/feedback1.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="//netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js" ></script> 

<script src="//code.jquery.com/jquery-2.1.4.min.js"></script>

<script src="../js/jquery.star-rating-svg.js"></script>

<script>

  function openSlideMenu(){
    document.getElementById('menu').style.width = '250px';
    document.getElementById('content').style.marginLeft = '250px';
    document.getElementById('navButton').style.display = 'none';
  }
  function closeSlideMenu(){
    document.getElementById('menu').style.width = '0';
    document.getElementById('content').style.marginLeft = '0';
    document.getElementById('navButton').style.display = 'block';
  }


</script>

<style>
  .close {
  color: #aaaaaa;
  float: right;
  font-size: 28px;
  font-weight: bold;
}

.close:hover,
.close:focus {
  color: #000;
  text-decoration: none;
  cursor: pointer;
}
.modal-content {
  background-color: rgb(50, 169, 238);
  margin: auto;
  padding: 20px;
  border: 1px solid #888;
  width: 80%;
}
.modal {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  padding-top: 100px; /* Location of the box */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

</style>

</head>
<body>
	<div class = "navbar">
		<div id = "logo">
		</div>
	</div>
  <div id="content">
    <span class="slide" id="navButton">
      <a href="#" onclick="openSlideMenu()">
        <i class="fas fa-bars"></i>
      </a>
    </span>

    <div id="menu" class="nav">
      <a href="#" class="close" onclick="closeSlideMenu()">
        <i class="fas fa-times"></i>
      </a>
      <img id = "profile" src="../img/profile.jpg">
      <div id = "list">
      <a href="#">Dashboard</a>
      <a href="#">Portfolio</a>
      <a href="logout.php">Log out</a>
      <?php echo phpversion() ?>
  </div>
    </div>

    <h1 class = "upper-margin">Dashboard</h1>
    <div class = "container">

    <div class = "table-vertical">
    	<table class = "table table-bordered table-striped table-hover table-mc-light-blue">
    		<thead>
    			<tr>
    				<th>No</th>
    				<th>Feedback</th>
    				<th>Assignment</th>
    				<th>Points</th>
    				<th>Date</th>
    			</tr>
    		</thead>
    		<tbody>
        <?php
        if ($result){
          if (mysqli_num_rows($result) > 0){
            while($row = mysqli_fetch_assoc($result)){
              $assign= $row['assignment_id'];
              $assignment="SELECT * FROM `assignments` WHERE `id`='$assign' LIMIT 1";
              $Aresult = mysqli_query($conn, $assignment);
            ?>
      			<tr>
      				<td data-title="No"><?php echo $row['id']; ?></td>
              <?php
              if ($Aresult){
                if (mysqli_num_rows($Aresult) > 0){
                  while($rows = mysqli_fetch_assoc($Aresult)){?>
      				      <td data-title="Address"><?php echo $rows['assignment']; ?></td>
                  <?php       
                  }
                }
              }
              ?>
              <td data-title="Points">
              <?php
               if($row['points']==1){
                  ?><i class="fa fa-star checked" id="one"></i>
                    <i class="fa fa-star unchecked" id="two"></i>
                    <i class="fa fa-star unchecked" id="three"></i>
                    <i class="fa fa-star unchecked" id="four"></i>
                    <i class="fa fa-star unchecked" id="five"></i><?php
               }elseif ($row['points']==2) {
                    ?><i class="fa fa-star checked" id="one"></i>
                    <i class="fa fa-star checked" id="two"></i>
                    <i class="fa fa-star unchecked" id="three"></i>
                    <i class="fa fa-star unchecked" id="four"></i>
                    <i class="fa fa-star unchecked" id="five"></i><?php
               }elseif ($row['points']==3) {
                  ?><i class="fa fa-star checked" id="one"></i>
                  <i class="fa fa-star checked" id="two"></i>
                  <i class="fa fa-star checked" id="three"></i>
                  <i class="fa fa-star unchecked" id="four"></i>
                  <i class="fa fa-star unchecked" id="five"></i><?php
               }elseif ($row['points']==4) {
                  ?><i class="fa fa-star checked" id="one"></i>
                  <i class="fa fa-star checked" id="two"></i>
                  <i class="fa fa-star checked" id="three"></i>
                  <i class="fa fa-star checked" id="four"></i>
                  <i class="fa fa-star unchecked" id="five"></i><?php
               }elseif ($row['points']==5) {
                  ?><i class="fa fa-star checked" id="one"></i>
                  <i class="fa fa-star checked" id="two"></i>
                  <i class="fa fa-star checked" id="three"></i>
                  <i class="fa fa-star checked" id="four"></i>
                  <i class="fa fa-star checked" id="five"></i><?php
               } 
               ?>
              </td>
      				<td data-title="Date"><?php echo $row['date']; ?></td>
              <td data-title="Buttons"><img id = "send" src = "../img/send.png" width="30px" style="z-index: 1"></td>
      			</tr>
            <?php       
            }
          }
        }
        ?>
    		</tbody>
    	</table>
    </div>
</div>
  </div>

<div id ="myModal" class = "modal">
  <div class="modal-content">
   <span class="close">&times;</span>
  <div class="formPage">
                <div id="bannerbar"></div>
                <div id="barTwo"></div>
                <div>
                        <img id ="logo-feedback" src="../img/UMClogo.svg" alt="UMC logo image">
                </div>
                <p>
                    <h3 id="title"> New Feedback form </h3>
                </p>
                <div class="sectionLine"></div>
                <div id="studentDetails">
                    <table>
                        <tr>
                            <th><p>
                                    <h4>Student name: </h4><br>
                                    <input type="text" placeholder="Name">
                                </p></th>
                            <th>    
                                <p>
                                    <h4 id = "header-margin">Student number: </h4><br>
                                    <input type="text" placeholder="Student Number">
                                </p></th>
                             <th>
                                 <p id="container">
                                        <img id ="userProfile" src="../img/profile.png" alt="user profile image">
                                 </p>
    
                                </th>
                        </tr>
                    </table>
                </div>
                <div class="sectionLine"></div>
                <div class ="textFeedback" contenteditable>
                    <h4>Please enter or say the feed back that you want to send to the student</h4>
                    <textarea name="feedback" id="feedback" cols="30" rows="10"></textarea>
                    

                </div>
                <button id="button" onclick="toggleStartStop()"></button>
                <script>
                        var recognizing;
                        window.SpeechRecognition = window.SpeechRecognition || window.webkitSpeechRecognition;
                        const recognition = new SpeechRecognition();
                        recognition.interimResults = true;
                        recognition.lang = 'en-US';
                        
                        recognition.continuous = true;
                        reset();
                        recognition.onend = reset();

                        // recognition.onresult = function (event) {
                        // for (var i = event.resultIndex; i < event.results.length; ++i) {
                        //     if (event.results[i].isFinal) {
                        //     textFeedback.value += event.results[i][0].transcript;
                        //     }
                        // }
                        // }

                        function reset() {
                        recognizing = false;
                        button.innerHTML = "Click to Speak";
                        document.getElementById("button").style.background = "green";
                        }

                        function toggleStartStop() {
                        if (recognizing) {
                            
                            recognition.stop();
                            // recognizing = false;
                            reset();
                        } else {
                            recognition.start();
                            recognizing = true;
                            button.innerHTML = "Click to Stop";
                            document.getElementById("button").style.background = "red";
                        }
                        }

                        //let p = document.createElement('p');
                        let p = document.getElementById('feedback');
                        const words = document.querySelector('.textFeedback');
                        words.appendChild(p);
                        recognition.addEventListener('result', e => {
                          const transcript = Array.from(e.results)
                            .map(result => result[0])
                            .map(result => result.transcript)
                            .join('');
                            const poopScript = transcript.replace(/poop|poo|shit|dump/gi, '💩');
                            p.textContent = poopScript;
                            if (e.results[0].isFinal) {
                             // p = document.createElement('p');
                              let p = document.getElementById('feedback');
                              words.appendChild(p);
                            }
                        });
                        // recognition.addEventListener('end', recognition.start);
                        //recognition.start();
                      </script>
</div>
        </div>
</div>

<script>
// Get the modal
let modal = document.getElementById("myModal");

// Get the button that opens the modal
let btn = document.getElementById("send");

let span = document.getElementsByClassName("close")[0];
// When the user clicks the button, open the modal 
btn.onclick = function() {
  modal.style.display = "block";
  console.log("open modal");
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  console.log("open span");
  modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}
</script>

</body>
</html>

















