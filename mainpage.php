<?php
	session_start();
	include('connect.php');
	$user=$_SESSION['user_id'];
  $class=$_SESSION['class_id'];
  $query="SELECT * FROM `feedback` WHERE `user_id`='$user'";
  $result = mysqli_query($conn, $query);
?>
<!DOCTYPE html>
<html>
<head>
  <title>Scorion | Portfolio</title>
  <script defer src="https://use.fontawesome.com/releases/v5.0.7/js/all.js"></script>
  <link rel="stylesheet" href="../PHP/css/style.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

  <link href="//netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js" ></script> 

<script src="//code.jquery.com/jquery-2.1.4.min.js"></script>

<script src="../js/jquery.star-rating-svg.js"></script>

<script>

  function openSlideMenu(){
    document.getElementById('menu').style.width = '250px';
    document.getElementById('content').style.marginLeft = '250px';
    document.getElementById('navButton').style.display = 'none';
  }
  function closeSlideMenu(){
    document.getElementById('menu').style.width = '0';
    document.getElementById('content').style.marginLeft = '0';
    document.getElementById('navButton').style.display = 'block';
  }

 
</script>
</head>
<body>
	<div class = "navbar">
		<div id = "logo">
		</div>
	</div>
  <div id="content">
    <span class="slide" id="navButton">
      <a href="#" onclick="openSlideMenu()">
        <i class="fas fa-bars"></i>
      </a>
    </span>

    <div id="menu" class="nav">
      <a href="#" class="close" onclick="closeSlideMenu()">
        <i class="fas fa-times"></i>
      </a>
      <img id = "profile" src="../img/profile.jpg">
      <div id = "list">
      <a href="#">Dashboard</a>
      <a href="#">Portfolio</a>
      <a href="logout.php">Log out</a>
  </div>
    </div>

    <h1 class = "upper-margin">Portfolio</h1>
    <div class = "container">

    <div class = "table-vertical">
    	<table class = "table table-bordered table-striped table-hover table-mc-light-blue">
    		<thead>
    			<tr>
    				<th>No</th>
    				<th>Feedback</th>
    				<th>Assignment</th>
    				<th>Points</th>
    				<th>Date</th>
    			</tr>
    		</thead>
    		<tbody>
        <?php
        if ($result){
          if (mysqli_num_rows($result) > 0){
            while($row = mysqli_fetch_assoc($result)){
              $assign= $row['assignment_id'];
              $assignment="SELECT * FROM `assignments` WHERE `id`='$assign' LIMIT 1";
              $Aresult = mysqli_query($conn, $assignment);
            ?>
      			<tr>
      				<td data-title="No"><?php echo $row['id']; ?></td>
      				<td data-title="Name"><?php echo $row['feedback']; ?></td>
              <?php
              if ($Aresult){
                if (mysqli_num_rows($Aresult) > 0){
                  while($rows = mysqli_fetch_assoc($Aresult)){?>
      				      <td data-title="Address"><?php echo $rows['assignment']; ?></td>
                  <?php       
                  }
                }
              }
              ?>
              <td data-title="Gender">
              <?php
               if($row['points']==1){
                  ?><i class="fa fa-star checked" id="one"></i>
                    <i class="fa fa-star unchecked" id="two"></i>
                    <i class="fa fa-star unchecked" id="three"></i>
                    <i class="fa fa-star unchecked" id="four"></i>
                    <i class="fa fa-star unchecked" id="five"></i><?php
               }elseif ($row['points']==2) {
                    ?><i class="fa fa-star checked" id="one"></i>
                    <i class="fa fa-star checked" id="two"></i>
                    <i class="fa fa-star unchecked" id="three"></i>
                    <i class="fa fa-star unchecked" id="four"></i>
                    <i class="fa fa-star unchecked" id="five"></i><?php
               }elseif ($row['points']==3) {
                  ?><i class="fa fa-star checked" id="one"></i>
                  <i class="fa fa-star checked" id="two"></i>
                  <i class="fa fa-star checked" id="three"></i>
                  <i class="fa fa-star unchecked" id="four"></i>
                  <i class="fa fa-star unchecked" id="five"></i><?php
               }elseif ($row['points']==4) {
                  ?><i class="fa fa-star checked" id="one"></i>
                  <i class="fa fa-star checked" id="two"></i>
                  <i class="fa fa-star checked" id="three"></i>
                  <i class="fa fa-star checked" id="four"></i>
                  <i class="fa fa-star unchecked" id="five"></i><?php
               }elseif ($row['points']==5) {
                  ?><i class="fa fa-star checked" id="one"></i>
                  <i class="fa fa-star checked" id="two"></i>
                  <i class="fa fa-star checked" id="three"></i>
                  <i class="fa fa-star checked" id="four"></i>
                  <i class="fa fa-star checked" id="five"></i><?php
               } 
               ?>
              </td>
      				<td data-title="Phone"><?php echo $row['date']; ?></td>
      				
      			</tr>
            <?php       
            }
          }
        }
        ?>
    		</tbody>
    	</table>
    </div>
</div>
  </div>


</body>

<script type="text/javascript">
  window.onscroll = function() {myFunction()};
let header = document.getElementById("logo");

// Get the offset position of the navbar
let sticky = header.offsetTop;
console.log(sticky + "");
// Add the sticky class to the header when you reach its scroll position. Remove "sticky" when you leave the scroll position
function myFunction() {
  if (window.pageYOffset > sticky) {
    header.classList.add("sticky");
  } else {
    header.classList.remove("sticky");
  }
}
</script>

</html>

















