<?php
	session_start();
	include('connect.php');
	$query = 'SELECT * FROM classes ORDER by id ASC';
    $result = mysqli_query($conn, $query);
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Scorpion | Login</title>
	<link rel="stylesheet" type="text/css" href="../css/style.css">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<section>
	<form  class = "box" method="post" action="login.php">
		<h1>Login</h1>
		<input type="text" name="uname" placeholder="Username"/>
		<input type="password" name="pass" placeholder="Password"/>
		<input class = "btn" type="submit" name="submit" value="Log In"/>
	</form>
</section>
	
</body>
</html>